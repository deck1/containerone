from django.shortcuts import render
import io
import json
import os
import subprocess
from rest_framework.decorators import api_view
from django.http.response import JsonResponse

processname='notebooks/b01.finiqs.ccf.elt_transformation_d1c1.0.0.2.final.py'

@api_view(['GET'])
def test(request):
    try:
        print("hello")
    except Exception as e:
        print(e)
    return JsonResponse({'message': 'Done'}, status=200)


@api_view(['GET'])
def pipeline(request):
    try:
        path=processname
        dir=os.path.dirname(__file__)
        filename = os.path.join(dir, path)
        subprocess.Popen(["python",filename])
    except Exception as e:
        print(e)
    return JsonResponse({'message': 'Done'}, status=200)