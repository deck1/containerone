# %%
"""
# Import Required Libraries
"""

# %%
import pandas as pd
import numpy as np
import boto3
import io
from datetime import datetime, timedelta
from multiprocessing import Pool
import atexit
import io
import logging

# %%
s3 = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com") 

# %%
"""
# Import Libraries which are only meaningful when working in ipython notebook
"""

# %%
"""
# Data Transformation E(T)L
"""

# %%
"""
### Extract an Individual Customer's all available data
"""

# %%
def cx_detail(cx, contract_no):
    df_cx_bill=df_bill.loc[df_bill['CONTRACT_NO'].isin([contract_no])].sort_values(by=['BILL_DATE'])
    df_cx_payment=df_payment.loc[df_payment['CONTRACT_NO'].isin([contract_no])].sort_values(by=['PAYMENT_DATE'])
    df_cx_schedule=df_schedule.loc[df_schedule['CONTRACT_NO'].isin([contract_no])].sort_values(by=['RENTAL_NO'])
    df_cx_loan=df_loan.loc[df_loan['CONTRACT_NO'].isin([contract_no])]
    df_cx_borrower=df_borrower.loc[df_borrower['BORROWER_ID'].isin([cx])]
    df_cx_crm=df_crm.loc[df_crm['BORROWER_ID'].isin([cx])]
    
    cx_data={}

    dict_cx_loan = df_cx_loan.to_dict('records')
    dict_cx_schedule = df_cx_schedule.to_dict('records')
    dict_cx_payment = df_cx_payment.to_dict('records')
    dict_cx_bill = df_cx_bill.to_dict('records')
    dict_cx_borrower = df_cx_borrower.to_dict('records')
    dict_cx_crm = df_cx_crm.to_dict('records')
    
    dict_cx={}
    dict_cx['loan']=dict_cx_loan
    dict_cx['schedule']=dict_cx_schedule
    dict_cx['payment']=dict_cx_payment
    dict_cx['bill']=dict_cx_bill
    dict_cx['borrower']=dict_cx_borrower
    dict_cx['crm']=dict_cx_crm
    cx_data[cx]=dict_cx

    return cx_data

# %%
def get_billdate(row, cx_detail_dict,cx):
    bill_gen_date=[]
    for d in cx_detail_dict[cx]['bill']:
        bill_gen_date.append(d['BILL_DATE'])
    if((row['RENTAL_NO']) <= len(bill_gen_date)):
        return pd.to_datetime(bill_gen_date[row['RENTAL_NO']-1])
    else:
        return pd.to_datetime(row['DUE_DATE'])

# %%
def get_due_date(row, cx_detail_dict,cx):
    for d in cx_detail_dict[cx]['bill']:
        if(row['BILL_DATE']==d['BILL_DATE']):
            return pd.to_datetime(d['PAYMENT_DUE_DATE'])

# %%
def get_billed_amount(row, cx_detail_dict,cx):
    for d in cx_detail_dict[cx]['bill']:
        if(row['BILL_DATE']==d['BILL_DATE']):
            return d['TRANSACTION_AMOUNT']

# %%
def get_previous_cum_bill_gen(row, df_cx):
    index=row['RENTAL_NO']-1
    if(index==0):
        return 0.0 
    else:
        prev=0.0
        for seq in range(index):
            prev=prev+df_cx.iloc[seq]['BILLED_AMT']
        return prev

# %%
def get_next_sch(index, df_cx):
    return pd.to_datetime(df_cx.iloc[index]['DUE_DATE'])

# %%
def get_current_rental_payment(row,cx_detail_dict, df_cx,cx):
    index=row['RENTAL_NO']
    if(len(cx_detail_dict[cx]['payment'])==0):
        return 0
    else:
        payment_df=pd.DataFrame(cx_detail_dict[cx]['payment'])
        payment_df['PAYMENT_DATE']=pd.to_datetime(payment_df['PAYMENT_DATE'])
        if(len(df_cx) > index):
            current_rental_payment=0.0
            start_date=pd.to_datetime(row["DUE_DATE"])
            next_sch=get_next_sch(index,df_cx)     
            payment_df_new = payment_df[(payment_df['PAYMENT_DATE'] >= start_date) 
                                        & (payment_df['PAYMENT_DATE'] < next_sch)]
            for index,row in payment_df_new.iterrows():
                current_rental_payment=current_rental_payment+row["PAID_AMOUNT"]
                pass
            
            return current_rental_payment

# %%
def get_current_rental_payment_details(row,cx_detail_dict,df_cx,cx):
    index=row['RENTAL_NO']
    if(len(cx_detail_dict[cx]['payment'])==0):
        return "||"
    else:
        payment_df=pd.DataFrame(cx_detail_dict[cx]['payment'])
        payment_df['PAYMENT_DATE']=pd.to_datetime(payment_df['PAYMENT_DATE'])
        if(len(df_cx) > index):
            current_rental_payment_detail="||"
            start_date=pd.to_datetime(row["DUE_DATE"])
            next_sch=get_next_sch(index,df_cx)     
            payment_df_new = payment_df[(payment_df['PAYMENT_DATE'] >= start_date) 
                                        & (payment_df['PAYMENT_DATE'] < next_sch)]
            for index,row in payment_df_new.iterrows():
                current_rental_payment_detail=current_rental_payment_detail+row["PAYMENT_DATE"].strftime('%Y-%m-%d')+"#"+str(row["PAID_AMOUNT"])+"||"
                pass
            
            return current_rental_payment_detail

# %%
def get_previous_cum_bill_pay(row,cx_detail_dict,cx):   
    if(len(cx_detail_dict[cx]['payment'])==0):
        return 0
    else:   
        bill_dt=row['BILL_DATE']
        prev=0
        for x in cx_detail_dict[cx]['payment']:
            if(x['PAYMENT_DATE'] < bill_dt):
                prev=prev+x['PAID_AMOUNT']
                pass
            pass       
        return prev

# %%
def get_payment_status(row, cx_detail_dict,cx):    
    index=row['RENTAL_NO']-1
    if(index<len(cx_detail_dict[cx]['payment'])):
        return cx_detail_dict[cx]['payment'][index]['PAYMENT_STATUS']
    else:
        return'N'

# %%
def fct_cx_detail(cx, cx_detail_dict,df_cx):
    df_cx['ISSUE_DATE']=cx_detail_dict[cx]['loan'][0]['DATE_OF_ISSUE']
    df_cx['PRINCIPAL_AMT']=cx_detail_dict[cx]['loan'][0]['PRINCIPAL_AMOUNT']
    df_cx['INT_RATE']=cx_detail_dict[cx]['loan'][0]['INTEREST_RATE']
    df_cx['INT_TYPE']=cx_detail_dict[cx]['loan'][0]['INTEREST_TYPE']
    df_cx['LOAN_NAME']=cx_detail_dict[cx]['loan'][0]['LOAN_NAME']
    df_cx['LOAN_TYPE']=cx_detail_dict[cx]['loan'][0]['LOAN_TYPE']
    df_cx['MORTAGE_TYPE']=cx_detail_dict[cx]['loan'][0]['MORTAGE_TYPE']
    df_cx['MORTAGE_AMOUNT']=cx_detail_dict[cx]['loan'][0]['MORTAGE_AMOUNT']
    
    df_cx['PAYMENT_STATUS']=df_cx.apply(get_payment_status, cx_detail_dict=cx_detail_dict,cx=cx,axis=1)

    df_cx['NO_OF_DAYS_IN_ARREARS']=cx_detail_dict[cx]['loan'][0]['NO_OF_DAYS_IN_ARREARS']
    df_cx['LOAN_CURRENT_STATUS']=cx_detail_dict[cx]['loan'][0]['LOAN_STATUS']
    df_cx['MOBILE_NO']=cx_detail_dict[cx]['borrower'][0]['MOBILE_NO']
    df_cx['DATE_OF_BIRTH']=cx_detail_dict[cx]['borrower'][0]['DATE_OF_BIRTH']
    df_cx['AGE']=cx_detail_dict[cx]['borrower'][0]['AGE']
    df_cx['GENDER']=cx_detail_dict[cx]['borrower'][0]['GENDER']
    df_cx['MARITAL_STATUS']=cx_detail_dict[cx]['borrower'][0]['MARITAL_STATUS']
    df_cx['ADDRESS']=cx_detail_dict[cx]['borrower'][0]['ADDRESS_LINE_3']
    df_cx['DISTRICT_NAME']=cx_detail_dict[cx]['borrower'][0]['DISTRICT_NAME']
    df_cx['PROVINCE_NAME']=cx_detail_dict[cx]['borrower'][0]['PROVINCE_NAME']
    
    if(len(cx_detail_dict[cx]['crm'])>0):
        df_cx['AGE_OF_RELATION_MONTH']=cx_detail_dict[cx]['crm'][0]['AGE_OF_RELATION_MONTHS']
        df_cx['TOTAL_NO_OF_LOANS']=cx_detail_dict[cx]['crm'][0]['TOTAL_NO_OF_LOANS']
        df_cx['TOTAL_LOAN_AMOUNT']=cx_detail_dict[cx]['crm'][0]['TOTAL_LOAN_AMOUNT']
    else:
        df_cx['AGE_OF_RELATION_MONTH']= np.nan
        df_cx['TOTAL_NO_OF_LOANS']=np.nan
        df_cx['TOTAL_LOAN_AMOUNT']=np.nan
        pass
    
    df_cx['TOT_PAYABLE']=cx_detail_dict[cx]['loan'][0]['EMI_AMOUNT']*cx_detail_dict[cx]['loan'][0]['LOAN_PERIOD']
    df_cx['DISTRICT NAME']=cx_detail_dict[cx]['borrower'][0]['DISTRICT_NAME']
    df_cx['BILL_DATE']=df_cx.apply(get_billdate, cx_detail_dict=cx_detail_dict,cx=cx, axis=1)
    df_cx['BILL_DUE_DATE']=df_cx.apply(get_due_date, cx_detail_dict=cx_detail_dict,cx=cx, axis=1)
    df_cx['BILL_DUE_DATE'].fillna(value=pd.to_datetime('2000-01-01'),inplace=True)
    df_cx['BILLED_AMT']=df_cx.apply(get_billed_amount, cx_detail_dict=cx_detail_dict,cx=cx, axis=1)
    df_cx['BILLED_AMT'].fillna(value=df_cx.iloc[0]['DUE_AMOUNT'],inplace=True)
    df_cx['PREV_CUM_BILL']=df_cx.apply(get_previous_cum_bill_gen, df_cx=df_cx, axis=1)
    df_cx['CURRENT_RENTAL_PAYMENT']=df_cx.apply(get_current_rental_payment, cx_detail_dict=cx_detail_dict,df_cx=df_cx,cx=cx, axis=1)
    df_cx['CURRENT_RENTAL_PAYMENT'].fillna(value=0,inplace=True)
    
    df_cx['CURRENT_RENTAL_PAYMENT_DETAILS']=df_cx.apply(get_current_rental_payment_details,cx_detail_dict=cx_detail_dict,df_cx=df_cx, cx=cx, axis=1)
    
    df_cx['PREV_CUM_PAYMENT']=df_cx.apply(get_previous_cum_bill_pay,cx_detail_dict=cx_detail_dict,cx=cx,axis=1)

    df_cx.rename(columns={"CONTRACT_NO": "LOAN_ID","DUE_DATE":"SCHEDULE_DATE", "DUE_AMOUNT":"EMI"})
    
    return df_cx

# %%
"""
# Payment Status Transformation
"""

# %%
def paidup_ratio(row):
    if(float(row['PREV_CUM_BILL'])==0.0):
        return 0.0
    else:
        if(float(row['PREV_CUM_BILL']) == 0.0):
            return 0.0
        else:
            return float(row['PREV_CUM_PAYMENT'])/float(row['PREV_CUM_BILL'])
        pass

# %%
def payment_flag(row,rental_no,month):
    if(len(df_cx) >= rental_no):
        if(row['RENTAL_NO']==rental_no):
            if(row['PAID_UP_RATIO'] > 1.00):
                return 1
            else:
                if(row['CURRENT_RENTAL_PAYMENT']>0):
                    return 1
                else:
                    return -1
        else:
            if(row['RENTAL_NO'] < rental_no):
                return 0      
            else:
                if(df_cx.iloc[rental_no-1]['PAID_UP_RATIO']> 1.00):
                    return 1
                else:
                    if(df_cx.iloc[rental_no-1]['CURRENT_RENTAL_PAYMENT']>0):
                        return 1
                    else:
                        return -1
    else:
        return 0

# %%
def payment_flag_new(row,rental_no,month,df_cx):
    
    if(len(df_cx) >= rental_no):
        if(row['RENTAL_NO']==rental_no):
            checkpay = (row['PREV_CUM_PAYMENT'] - row['PREV_CUM_BILL']) - (0.90*row['BILLED_AMT'])
            
            if(checkpay>0):
                return 1
            else:
                if(row['CURRENT_RENTAL_PAYMENT']>0):
                    return 1
                else:
                    return -1
        else:
            if(row['RENTAL_NO'] < rental_no):
                return 0      
            else:
                checkprevpay = (df_cx.iloc[rental_no-1]['PREV_CUM_PAYMENT'] - df_cx.iloc[rental_no-1]['PREV_CUM_BILL']) - (0.90*df_cx.iloc[rental_no-1]['BILLED_AMT'])
                if(checkprevpay>0):
                    return 1
                else:
                    if(df_cx.iloc[rental_no-1]['CURRENT_RENTAL_PAYMENT']>0):
                        return 1
                    else:
                        return -1
    else:
        return 0

# %%
def get_till_now_delinq(row):
    cnt=0
    index=row['RENTAL_NO']-1
    if(index>0):
        for i in range(index):
            month='M'+str(i+1)
            if(row[month] == -1):
                cnt=cnt+1
    return cnt

# %%
def get_prev_due_balance(row):
    return row['PREV_CUM_BILL']-row['PREV_CUM_PAYMENT']

# %%
def get_chargeoff_within_3_mths(row):
    index=row['RENTAL_NO']
    cnt=0
    if(index-3>0):
        for i in range(index-3, index):
            month='M'+str(i)
            if(row[month] == -1):
                cnt=cnt+1
    else:
        if(index==2):
            for i in range(index-1, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==3):
            for i in range(index-2, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
    return cnt

# %%
def get_chargeoff_within_6_mths(row):
    index=row['RENTAL_NO']
    cnt=0
    if(index-6>0):
        for i in range(index-6, index):
            month='M'+str(i)
            if(row[month] == -1):
                cnt=cnt+1
    else:
        if(index==2):
            for i in range(index-1, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==3):
            for i in range(index-2, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==4):
            for i in range(index-3, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==5):
            for i in range(index-4, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==6):
            for i in range(index-5, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
    return cnt

# %%
def get_chargeoff_within_12_mths(row):
    index=row['RENTAL_NO']
    cnt=0
    if(index-12>0):
        for i in range(index-12, index):
            month='M'+str(i)
            if(row[month] == -1):
                cnt=cnt+1
    else:
        if(index==2):
            for i in range(index-1, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==3):
            for i in range(index-2, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==4):
            for i in range(index-3, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==5):
            for i in range(index-4, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==6):
            for i in range(index-5, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==7):
            for i in range(index-6, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==8):
            for i in range(index-7, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==9):
            for i in range(index-8, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==10):
            for i in range(index-9, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==11):
            for i in range(index-10, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
        elif(index==12):
            for i in range(index-11, index):
                month='M'+str(i)
                if(row[month] == -1):
                    cnt=cnt+1
    
    return cnt

# %%
def get_collection_times_3_mths(row):
    index=row['RENTAL_NO']
    cnt=0
    if(index-3>0):
        for i in range(index-3, index):
            month='M'+str(i)
            if(row[month] == 1):
                cnt=cnt+1
    else:
        if(index==2):
            for i in range(index-1, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==3):
            for i in range(index-2, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
    return cnt

# %%
def get_collection_times_6_mths(row):
    index=row['RENTAL_NO']
    cnt=0
    if(index-6>0):
        for i in range(index-6, index):
            month='M'+str(i)
            if(row[month] == 1):
                cnt=cnt+1
    else:
        if(index==2):
            for i in range(index-1, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==3):
            for i in range(index-2, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==4):
            for i in range(index-3, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==5):
            for i in range(index-4, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==6):
            for i in range(index-5, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
                    
    return cnt

# %%
def get_collection_times_12_mths(row):
    index=row['RENTAL_NO']
    cnt=0
    if(index-12>0):
        for i in range(index-12, index):
            month='M'+str(i)
            if(row[month] == 1):
                cnt=cnt+1
    else:
        if(index==2):
            for i in range(index-1, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==3):
            for i in range(index-2, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==4):
            for i in range(index-3, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==5):
            for i in range(index-4, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==6):
            for i in range(index-5, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==7):
            for i in range(index-6, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==8):
            for i in range(index-7, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==9):
            for i in range(index-8, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==10):
            for i in range(index-9, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==11):
            for i in range(index-10, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1
        elif(index==12):
            for i in range(index-11, index):
                month='M'+str(i)
                if(row[month] == 1):
                    cnt=cnt+1

    return cnt

# %%
def get_dti(row,df_cx):
    rental=row['RENTAL_NO']-1
    current_payment=row['CURRENT_RENTAL_PAYMENT']
    billed_amt=row['BILLED_AMT']
    total_obli=row['PREV_CUM_BILL']-df_cx.iloc[rental]['PREV_CUM_PAYMENT']
    
    if(total_obli>0):
        if(billed_amt==0):
            return 0.0
        else:
            return total_obli/billed_amt
    else:
        if(total_obli==0):
            return 0.0
        else:
            if(billed_amt==0):
                return 0.0
            else:
                return total_obli/billed_amt

# %%
def get_prev_dti(row):
    prev_rental=row['RENTAL_NO']-1
    if(prev_rental>0):
        payment=df_cx.iloc[prev_rental]['CURRENT_RENTAL_PAYMENT']  
        total_obli=df_cx.iloc[prev_rental]['PREV_CUM_BILL']+df_cx.iloc[prev_rental-1]['PREV_CUM_PAYMENT']
        if(total_obli>0):
            if(total_obli==0):
                return 0.0
            else:
                return payment/total_obli
        else:
            if(total_obli==0):
                return 0.0
            else:
                return current_payment/total_obli
    else:
        return 0.0

# %%
def get_prev_dti_new(row):
    return 0.0

# %%
def get_last_pymnt_amnt(row,df_cx):
    prev_rental=row['RENTAL_NO']-1
    if(prev_rental>0):
        return df_cx.iloc[prev_rental-1]['CURRENT_RENTAL_PAYMENT']  
    else:
        return 0.0

# %%
def get_mths_since_last_delinq(row):
    rental=row['RENTAL_NO']
    rental=rental-1
    month_num=0
    while(rental>1):
        month_num=rental
        month='M'+str(rental)
        if(row[month]==-1):
            return month_num 
        else:
            rental=rental-1
        pass

# %%
def get_mths_since_last_delinq_new(row):
    rental=row['RENTAL_NO']
    rental=rental-1
    month_num=0
    month_num_for_last_dlq=0
    while(rental>1):
        month_num=rental
        month='M'+str(rental)
        if(row[month]==-1):
            return row['RENTAL_NO'] - rental 
        else:
            rental=rental-1
    
    return 0

# %%
def get_out_prncp(row):
    return row['TOT_PAYABLE']-row['PREV_CUM_PAYMENT']

# %%
def get_out_prncp_new(row):
    if(row['TOT_PAYABLE']==0):
        return 0.0
    else:
        return (row['TOT_PAYABLE']-row['PREV_CUM_PAYMENT'])/row['TOT_PAYABLE']*1.0

# %%
"""
# Calculate Transformed Columns
"""

# %%
"""
#### Prepare for every individual Borrower
"""

# %%
from io import StringIO

# %%
"""
## Logs 
"""

# %%
def write_logs(body,file_path):
    try:
   
        pass
    except Exception as e:
        print(e)
        pass
   
    

# %%
def transform(sample_size,start,end,time,process,sn):
    s3_log = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com")
    try:
        start_time=datetime.now()    
        cx_array=df_borrower.iloc[start:end]['BORROWER_ID'].to_numpy()
        transformed_df=pd.DataFrame()
        max_term=df_loan.LOAN_PERIOD.max()
        df_cx=None
        total_cx=len(cx_array)
        for cx in cx_array:
            print("Processing...")
            print("\tBORROWER: ",cx)

            check_df_cx = df_schedule.loc[df_schedule['BORROWER_ID'].isin([cx])].sort_values(by=['RENTAL_NO'])
            if(check_df_cx.empty):
                total_cx=total_cx-1
                print("Skipped! - - - Empty Borrower!")
                continue

            borrower_contracts = df_loan[df_loan.BORROWER_ID.isin([cx])]['CONTRACT_NO'].array
            for contract in borrower_contracts:
                print('\t\tCONTRACTS: ',contract)

                cx_detail_dict=cx_detail(cx, contract)
                df_cx = df_schedule.loc[df_schedule['CONTRACT_NO'].isin([contract])].sort_values(by=['RENTAL_NO'])

                if(df_cx.empty):
                    print("Skipped! - - - Empty Contract!")
                    continue

                df_cx=fct_cx_detail(cx, cx_detail_dict,df_cx)
                df_cx['PAID_UP_RATIO']=0.00
                df_cx['PAID_UP_RATIO']=df_cx.apply(paidup_ratio,axis=1)

                for i in range(max_term):
                    monthname='M'+str(i+1)
                    df_cx[monthname]=0
                    df_cx[monthname]=df_cx.apply(payment_flag_new,rental_no=i+1,month=monthname,df_cx=df_cx,axis=1)

                df_cx['TILL_NOW_DELINQ']=0
                df_cx['TILL_NOW_DELINQ']=df_cx.apply(get_till_now_delinq,axis=1)
                df_cx['PREV_DUE_BALANCE']=df_cx.apply(get_prev_due_balance,axis=1)
                df_cx['CHARGEOFF_WITHIN_3_MTHS']=df_cx.apply(get_chargeoff_within_3_mths,axis=1)
                df_cx['COLLECTION_TIMES_3_MTHS']=df_cx.apply(get_collection_times_3_mths,axis=1)
                df_cx['CHARGEOFF_WITHIN_6_MTHS']=df_cx.apply(get_chargeoff_within_6_mths,axis=1)
                df_cx['COLLECTION_TIMES_6_MTHS']=df_cx.apply(get_collection_times_6_mths,axis=1)
                df_cx['CHARGEOFF_WITHIN_12_MTHS']=df_cx.apply(get_chargeoff_within_12_mths,axis=1)
                df_cx['COLLECTION_TIMES_12_MTHS']=df_cx.apply(get_collection_times_12_mths,axis=1)
                df_cx['DTI']=df_cx.apply(get_dti,df_cx=df_cx,axis=1)
                df_cx['PREV_DTI']=df_cx.apply(get_prev_dti_new,axis=1)
                df_cx['LAST_PYMT_AMNT']=df_cx.apply(get_last_pymnt_amnt,df_cx=df_cx,axis=1)
                df_cx['MNTH_SINCE_LAST_DELINQ']=df_cx.apply(get_mths_since_last_delinq_new,axis=1)
                df_cx['MNTH_SINCE_LAST_DELINQ'].fillna(value=0,inplace=True)
                df_cx['OUT_PRNCP']=df_cx.apply(get_out_prncp,axis=1)
                transformed_df=transformed_df.append(df_cx)

            total_cx=total_cx-1
            print("The process running : " + process)
            print("Remaining :" + str(total_cx))

        end_time=datetime.now()
        lapsed_time=end_time-start_time
        tm="Time Spent by "+ process + " :"
        print(tm ,lapsed_time)
        print("Saving...")
        
        
        file_path3='transformed_saving_'+"D1C1"+"_sn_"+sn+"_"+time+"_cx_log.csv"
        body2="Saving for - D1C1 \n SN - " + sn +"\n lines -" + time + "\n"
        s3_log.Object('logcontainerone-fdotlxhe',file_path3).put(Body=body2)
        
        file_path='transformed_'+"D1C1"+"_"+"1"+"_"+"1000"+"_"+"10001"+"_sn_"+sn+"_"+time+"_cx.csv"
        pickle_buffer = StringIO()
        transformed_df.to_csv(pickle_buffer)
        s3_2 = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com") 
        s3_2.Object('dcone-cazwymqj',file_path).put(Body=pickle_buffer.getvalue())
        
        print("Saving...Done")
        print("---------------------------------------------")
        file_path2='transformed_saved_'+"D1C1"+"_sn_"+sn+"_"+time+"_cx_log.csv"
        body="Saving...Done for - D1C1 \n SN - " + sn +"\n lines -" + time + "\n" + "time take  : " + str(tm) + ":" + str(lapsed_time)
        s3_log.Object('logcontainerone-fdotlxhe',file_path2).put(Body=body)
        pass
    except Exception as e:
        bodyexception=e
        file_path_ex='transformed_exception_'+"D1C1"+"_sn_"+sn+"_"+time+"_cx_log.csv"
        s3_log.Object('logcontainerone-fdotlxhe',file_path_ex).put(Body=bodyexception)
        print(e)

# %%
dataarray=[
    
{
    "SN":'1',
    "crm":'crm_extracted_set_51last.pickle',
    "bill":'bill_extracted_set_51last.pickle',
    "borrower":'borrower_extracted_set_51last.pickle',
    "loan":'loan_extracted_set_51last.pickle',
    "payment":'payment_extracted_set_51last.pickle',
    "sche":'schedule_extracted_set_51last.pickle',
}
]

# %%
"""
# Load Extracted DataFrame
"""

# %%
s3_log = boto3.resource('s3',region_name="eu-west-1",aws_access_key_id='VBLLZPX15YE3WMLBDO4A',aws_secret_access_key='v2cdWe6prisvmj1rwlEaTwUgh5NCH4gEvTYE8BJX', endpoint_url="https://kcs3.eu-west-1.klovercloud.com")
try:
    for data in dataarray:
        print("Starting for SN :" +str(data['SN']))
     
        print("----------starting to crm-------------")
        datacrm=s3.Object('containerlast-sutqohvh',data['crm']).get()['Body'].read()
        df_crm=pd.read_pickle(io.BytesIO(datacrm),compression=None)

        print("----------starting to borrower-------------")
        databorrow=s3.Object('containerlast-sutqohvh',data['borrower']).get()['Body'].read()

        databorrow=s3.Object('containerlast-sutqohvh',data['borrower']).get()['Body'].read()
        df_borrower = pd.read_pickle(io.BytesIO(databorrow),compression=None)


        print("----------starting to loan-------------")
        dataloan=s3.Object('containerlast-sutqohvh',data['loan']).get()['Body'].read()
        df_loan=pd.read_pickle(io.BytesIO(dataloan),compression=None)

        print("----------starting to pay-------------")
        datapay=s3.Object('containerlast-sutqohvh',data['payment']).get()['Body'].read()
        df_payment=pd.read_pickle(io.BytesIO(datapay),compression=None)

        print("----------starting to bill-------------")
        databill=s3.Object('containerlast-sutqohvh', data['bill']).get()['Body'].read()
        df_bill=pd.read_pickle(io.BytesIO(databill),compression=None)

        print("----------starting to schedule-------------")
        datasche=s3.Object('containerlast-sutqohvh',data['sche']).get()['Body'].read()
        df_schedule=pd.read_pickle(io.BytesIO(datasche),compression=None)
        print("----------Done all load-------------")
        
        
        pool = Pool(processes=6)
        pool.apply_async(transform, [10,5000,5786,'5000_5786','Process SIx',str(data['SN'])])
        pool.close()
        pool.join()
    pass
except Exception as e:
    bodyexception=e
    file_path_ex='transformed_exception_'+"D1C1"+"_cx_log.csv"
    s3_log.Object('logcontainerone-fdotlxhe',file_path_ex).put(Body=bodyexception)
    pass

# %%
